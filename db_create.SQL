SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `Employee_Management`.`Login`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Employee_Management`.`Login` (
  `UUID` CHAR NOT NULL,
  `password` VARCHAR(45) NULL,
  `role` ENUM('admin','employee') NULL,
  `Logincol` VARCHAR(45) NULL,
  `created_by` VARCHAR(45) NULL,
  `created_on` DATETIME NULL,
  `last_updated_by` VARCHAR(45) NULL,
  `last_updated_on` DATETIME NULL,
  `email_id` VARCHAR(45) NULL,
  PRIMARY KEY (`UUID`),
  UNIQUE INDEX `idLogin_UNIQUE` (`UUID` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Employee_Management`.`emp_personal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Employee_Management`.`emp_personal` (
  `UUID` CHAR NOT NULL,
  `employee_id` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `email_id` VARCHAR(45) NOT NULL,
  `DOB` DATE NOT NULL,
  `DOJ` DATE NOT NULL,
  `employement_status` ENUM('retired','employeed') NOT NULL,
  `created_by` VARCHAR(45) NOT NULL,
  `created_on` DATETIME NOT NULL,
  `last_updated_by` VARCHAR(45) NOT NULL,
  `last_updated_on` DATETIME NOT NULL,
  PRIMARY KEY (`UUID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Employee_Management`.`emp_address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Employee_Management`.`emp_address` (
  `UUID` INT NOT NULL,
  `type` ENUM('temporary','permanent') NOT NULL,
  `street_address` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `state` VARCHAR(45) NULL,
  `country_code` INT(2) NULL,
  `zip_code` VARCHAR(45) NULL,
  `created_by` VARCHAR(45) NULL,
  `created_on` DATETIME NULL,
  `last_updated_by` VARCHAR(45) NULL,
  `last_updated_on` DATETIME NULL,
  PRIMARY KEY (`UUID`, `type`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Employee_Management`.`emp_family`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Employee_Management`.`emp_family` (
  `UUID` INT NOT NULL,
  `full_name` VARCHAR(45) NOT NULL,
  `relation` ENUM('0','1') NULL,
  `dependent` VARCHAR(45) NULL,
  `DOB` DATE NULL,
  `blood_group` VARCHAR(45) NULL,
  PRIMARY KEY (`UUID`, `full_name`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Employee_Management`.`emp_communication`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Employee_Management`.`emp_communication` (
  `UUID` INT NOT NULL,
  `type` ENUM('sec-mail','skype','phone') NOT NULL,
  `details` VARCHAR(45) NULL,
  `created_by` VARCHAR(45) NULL,
  `created_on` DATETIME NULL,
  `last_updated_by` VARCHAR(45) NULL,
  `last_updated_on` DATETIME NULL,
  PRIMARY KEY (`UUID`, `type`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Employee_Management`.`attendance_register`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Employee_Management`.`attendance_register` (
  `UUID` INT NOT NULL,
  `Date` DATE NOT NULL,
  `status` ENUM('csv_uploaded','holiday','all_present') NULL,
  `created_by` VARCHAR(45) NULL,
  `created_on` DATETIME NULL,
  `last_updated_by` VARCHAR(45) NULL,
  `last_updated_on` DATETIME NULL,
  PRIMARY KEY (`UUID`, `Date`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Employee_Management`.`attendance_task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Employee_Management`.`attendance_task` (
  `Date` INT NOT NULL,
  `status` VARCHAR(45) NULL,
  `action` VARCHAR(45) NULL,
  `action_done_by` VARCHAR(45) NULL,
  `action_done_on` VARCHAR(45) NULL,
  PRIMARY KEY (`Date`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
