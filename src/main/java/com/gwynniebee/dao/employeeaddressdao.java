package com.gwynniebee.dao;

import java.util.ArrayList;
import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.address_details;
import com.gwynniebee.mappers.employeeaddressMapper;

@RegisterMapper(employeeaddressMapper.class)
public interface employeeaddressdao extends Transactional<employeeaddressdao>{

    @SqlQuery("select * from emp_address where UUID=:uuid")
    List<address_details> sqlqueryaddress(@Bind("uuid") String uuid);
}
