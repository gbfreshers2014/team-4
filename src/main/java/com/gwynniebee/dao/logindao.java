package com.gwynniebee.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.loginresponseobject;
import com.gwynniebee.mappers.loginMapper;

@RegisterMapper(loginMapper.class)
public interface logindao extends Transactional<logindao>{
    
    @SqlQuery("select UUID,role from loginCredential where email_id=:emailid and password=:password")
    loginresponseobject authenticateuserandspecifyrole(@Bind("emailid") String emai , @Bind("password") String password);
}