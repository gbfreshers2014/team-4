package com.gwynniebee.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.mappers.EmpAddressDetailsMapper;
import com.gwynniebee.mappers.EmpCommunicationDetailsMapper;
import com.gwynniebee.mappers.EmpFamilyDetailsMapper;
import com.gwynniebee.mappers.EmpPersonalDetailsMapper;

public interface EmpPerDetaildao extends Transactional<EmpPerDetaildao> {

    @RegisterMapper(EmpPersonalDetailsMapper.class)
    @SqlQuery("select * from employee_personal_details where uuid=:ud")
    PersonalDetails getPersonalData(@Bind("ud") String ud);

    @RegisterMapper(EmpFamilyDetailsMapper.class)
    @SqlQuery("select * from employee_family_details where uuid=:ud")
    List<FamilyDetails> getFamilyData(@Bind("ud") String ud);

    @RegisterMapper(EmpCommunicationDetailsMapper.class)
    @SqlQuery("select * from employee_communication_details where uuid=:ud")
    List<CommunicationDetails> getCommunicationData(@Bind("ud") String ud);

    @RegisterMapper(EmpAddressDetailsMapper.class)
    @SqlQuery("select * from employee_address_details where uuid=:ud")
    List<AddressDetails> getAddressData(@Bind("ud") String ud);

    // @SqlUpdate(":qury")
    // int updateTableValues(@Bind("qury") String qury);
    @SqlUpdate("UPDATE employeePersonalDetails SET lastName=:lastName WHERE UUID=:uuid")
    int updateTableValues(@Bind("lastName") String lastName, @Bind("uuid") String uuid);

    @SqlUpdate("insert into employeePersonalDetails values (:UUID,:employeeId,:firstName,:lastName,:DOB,:DOJ,:employmentStatus,:created_on,:created_by,:last_updated_on,:last_updated_by)")
    int addNewData(@Bind("created_by") String created_by, @Bind("created_on") String created_on, @Bind("DOB") String DOB,
            @Bind("DOJ") String DOJ, @Bind("employeeId") String employeeId, @Bind("employmentStatus") String employmentStatus,
            @Bind("firstName") String firstName, @Bind("last_updated_by") String last_updated_by,
            @Bind("last_updated_on") String last_updated_on, @Bind("lastName") String lastName, @Bind("UUID") String UUID);

}
// @RegisterMapper(UpdateDetailsMapper.class)
//
