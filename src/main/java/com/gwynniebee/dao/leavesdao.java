package com.gwynniebee.dao;

import java.sql.Date;
import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.leavesdetails;
import com.gwynniebee.backoffice.restlet.resources.leavesresponse;
import com.gwynniebee.mappers.leavesMapper;

@RegisterMapper(leavesMapper.class)
public interface leavesdao extends Transactional<leavesdao>{
    @SqlQuery("select Date,status from attendance_register where UUID=:uuid and Date>:sd and Date<:ed")
    List<leavesdetails> queryleaves(@Bind("uuid")String uuid,@Bind("sd")Date startdate,@Bind("ed")Date enddate);
}
