package com.gwynniebee.dao;

import java.util.ArrayList;
import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.address_details;
import com.gwynniebee.backoffice.objects.family_details;
import com.gwynniebee.mappers.employeefamilyMapper;

@RegisterMapper(employeefamilyMapper.class)
public interface employeefamilydao extends Transactional<employeefamilydao> {
    @SqlQuery("select * from emp_family where UUID=:uuid")
    List<family_details> sqlqueryfamily(@Bind("uuid") String uuid);
}
