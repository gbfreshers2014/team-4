package com.gwynniebee.dao;

import java.util.ArrayList;
import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.searchResult;
import com.gwynniebee.backoffice.objects.searchResultArray;
import com.gwynniebee.backoffice.objects.address_details;
import com.gwynniebee.mappers.searchMapper;

@RegisterMapper(searchMapper.class)
public interface searchdao extends Transactional<searchdao>{

    @SqlQuery("select * from emp_personal where (UUID=:para OR first_name=:para OR last_name=:para OR email_id=:para)")
    List<searchResult> sqlquery(@Bind("para") String param);


}
