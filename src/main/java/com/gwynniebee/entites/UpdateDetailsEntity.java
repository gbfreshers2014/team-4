package com.gwynniebee.entites;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.StatusResponseObject;
import com.gwynniebee.backoffice.objects.UpdateInfoObject;
import com.gwynniebee.dao.EmpPerDetaildao;

//import com.gwynniebee.dao.logindao;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateDetailsEntity {

    public static void main(String[] a) throws ClassNotFoundException {
        UpdateDetailsEntity aaa = new UpdateDetailsEntity();
        UpdateInfoObject obj = new UpdateInfoObject();
        aaa.updateDetails(obj);

    }

    public StatusResponseObject updateDetails(UpdateInfoObject obj) throws ClassNotFoundException {
        Handle h = null;
        EmpPerDetaildao dao = null;
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi = new DBI("jdbc:mysql://localhost:3306/EmployeeManagement", "root", "iiit");
        // DBI dbi = new DBI("jdbc:mysql://localhost:1167/EmployeeManagement",
        // "root", "iiit");
        h = dbi.open();
        h.begin();
        dao = h.attach(EmpPerDetaildao.class);
        // // loginresponseobject tobereturned =

        StatusResponseObject toBeReturned = new StatusResponseObject();

        int flag = 0;
        String query = "UPDATE employeePersonalDetails SET";
        // if (obj.created_by.length() >= 1) {
        // query = query + " " + "created_by=\"" + obj.created_by + "\"";
        // flag = 1;
        // }
        // if (obj.created_on.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "created_on=" + obj.created_on;
        // } else {
        // query = query + " " + "created_on=" + obj.created_on;
        // }
        // flag = 1;
        // }
        // if (obj.DOB.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "DOB=" + obj.DOB;
        // } else {
        // query = query + " " + "DOB=" + obj.DOB;
        // }
        // flag = 1;
        // }
        //
        // if (obj.DOJ.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "DOJ=" + obj.DOJ;
        // } else {
        // query = query + " " + "DOJ=" + obj.DOJ;
        // }
        // flag = 1;
        // }
        //
        // if (obj.employeeId.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "employeeId=" + obj.employeeId;
        // } else {
        // query = query + " " + "employeeId=" + obj.employeeId;
        // }
        // flag = 1;
        // }
        //
        // if (obj.employeeStatus.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "employeeStatus=" + obj.employeeStatus;
        // } else {
        // query = query + " " + "employeeStatus=" + obj.employeeStatus;
        // }
        // flag = 1;
        // }

        if (obj.lastName.length() >= 1) {
            if (flag == 1) {
                query = query + "," + "lastName=" + obj.lastName;
            } else {
                query = query + " " + "lastName=" + obj.lastName;
            }
            flag = 1;
        }
        if (flag == 1) {
            query = query + " WHERE UUID=" + obj.UUID;
        }

        int output = 0;
        // if (flag != 0) {
        // String
        // qury="UPDATE employeePersonalDetails SET lastName=bain WHERE UUID=2";
        output = dao.updateTableValues(obj.lastName, obj.UUID);
        // output = dao.updateTableValues(qury);
        // System.out.println(output + "raa");
        // toBeReturned = dao.getTableValues("11");
        // toBeReturned.employeeId = "sda";
        // toBeReturned.firstName = "rahul sharmaaa";
        // }

        if (output == 0) {
            toBeReturned.status = 1;
            toBeReturned.message = "NOT_updated";

        } else {
            toBeReturned.status = 0;
            toBeReturned.message = "updated_successfully";
        }
        h.commit();
        h.close();
        // System.out.println(toBeReturned.employeeId);
        return toBeReturned;
    }
}
