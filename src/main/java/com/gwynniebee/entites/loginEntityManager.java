package com.gwynniebee.entites;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.loginObject;
import com.gwynniebee.backoffice.objects.loginresponseobject;
import com.gwynniebee.backoffice.restlet.resources.loginHandler;
import com.gwynniebee.dao.logindao;

public class loginEntityManager {

    public loginresponseobject authenticate(loginObject login_inst) throws ClassNotFoundException {
        Handle h=BaseEntityManager.getDBI();
        logindao dao=null;
        h.begin();
        dao = h.attach(logindao.class);        
       loginresponseobject tobereturned=dao.authenticateuserandspecifyrole(login_inst.emailid, login_inst.password);                
       h.commit();
       h.close();
       return tobereturned;     
    }

}
