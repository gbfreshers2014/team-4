package com.gwynniebee.entites;

import java.util.ArrayList;

import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.address_details;
import com.gwynniebee.backoffice.objects.communication_details;
import com.gwynniebee.backoffice.objects.family_details;
import com.gwynniebee.backoffice.objects.searchResult;
import com.gwynniebee.backoffice.objects.searchResultArray;
import com.gwynniebee.dao.employeeaddressdao;
import com.gwynniebee.dao.employeecommunicationdao;
import com.gwynniebee.dao.employeefamilydao;
import com.gwynniebee.dao.searchdao;

public class searchEntityManager {

    public searchResultArray searchQuery(String currentQueryParam) throws ClassNotFoundException {
        Handle h=BaseEntityManager.getDBI();
        searchdao dao=null;
        employeeaddressdao addrdao=null;
        employeefamilydao famdao=null;
        employeecommunicationdao cmddao=null;
        dao=h.attach(searchdao.class);
        famdao=h.attach(employeefamilydao.class);
        addrdao=h.attach(employeeaddressdao.class);
        cmddao=h.attach(employeecommunicationdao.class);
        h.begin();
        searchResultArray sra=new searchResultArray();
        sra.list=(ArrayList<searchResult>) dao.sqlquery(currentQueryParam);
        for(searchResult sr : sra.list)
        {
            sr.adrress=(ArrayList<address_details>) addrdao.sqlqueryaddress(sr.UUID);
            sr.communication=(ArrayList<communication_details>) cmddao.sqlquerycommunication(sr.UUID);
            sr.family=(ArrayList<family_details>) famdao.sqlqueryfamily(sr.UUID);
        }
        h.commit();
        h.close();
        return sra;
    }

}
