package com.gwynniebee.entites;

import java.sql.Date;
import java.util.ArrayList;

import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.leavesdetails;
import com.gwynniebee.backoffice.restlet.resources.leavesresponse;
import com.gwynniebee.dao.leavesdao;

public class leavesEntityManager {
    public static leavesresponse checkleaves(String uuid,Date startdate,Date enddate) throws ClassNotFoundException
    {
        Handle h=BaseEntityManager.getDBI();
        leavesdao dao;
        dao=h.attach(leavesdao.class);
        h.begin();
        leavesresponse leaves=new leavesresponse();
        leaves.leaves_details=(ArrayList<leavesdetails>) dao.queryleaves(uuid,startdate,enddate);
        h.close();
        return leaves;
    }
}
