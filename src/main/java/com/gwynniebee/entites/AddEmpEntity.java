package com.gwynniebee.entites;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.StatusResponseObject;
import com.gwynniebee.backoffice.restlet.resources.AddEmpObject;
import com.gwynniebee.dao.EmpPerDetaildao;

//import com.gwynniebee.dao.logindao;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddEmpEntity {

    public StatusResponseObject addEmployee(AddEmpObject obj) throws ClassNotFoundException {
        Handle h = null;
        EmpPerDetaildao dao = null;
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi = new DBI("jdbc:mysql://localhost:3306/EmployeeManagement", "root", "iiit");
        // DBI dbi = new DBI("jdbc:mysql://localhost:1167/EmployeeManagement",
        // "root", "iiit");
        h = dbi.open();
        h.begin();
        dao = h.attach(EmpPerDetaildao.class);
        // loginresponseobject tobereturned =
        //
        StatusResponseObject toBeReturned = new StatusResponseObject();

        int output = 0;
        output =
                dao.addNewData(obj.created_by, obj.created_on, obj.DOB, obj.DOJ, obj.employeeId, obj.employmentStatus, obj.firstName,
                        obj.last_updated_by, obj.last_updated_on, obj.lastName, obj.UUID);
        // toBeReturned = dao.getTableValues("11");
        // toBeReturned.employeeId = "sda";
        // toBeReturned.firstName = "rahul sharmaaa";

        if (output == 0) {
            toBeReturned.status = 1;
            toBeReturned.message = "NOT_updated";

        } else {
            toBeReturned.status = 0;
            toBeReturned.message = "Successful";
        }

        h.commit();
        h.close();
        // System.out.println(toBeReturned.employeeId);
        return toBeReturned;
    }
}
