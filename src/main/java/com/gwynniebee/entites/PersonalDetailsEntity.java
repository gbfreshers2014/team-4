package com.gwynniebee.entites;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.restlet.resources.Details;
import com.gwynniebee.dao.EmpPerDetaildao;

//import com.gwynniebee.dao.logindao;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonalDetailsEntity {

    public static void main(String[] a) throws JsonParseException, ClassNotFoundException, IOException {
        PersonalDetailsEntity aaaa = new PersonalDetailsEntity();
        aaaa.getPersonalInfo("1");

    }

    public Details getPersonalInfo(String uuid) throws ClassNotFoundException {
        Handle h = null;
        EmpPerDetaildao dao = null;
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi = new DBI("jdbc:mysql://induction-dev.gwynniebee.com:3306/t4", "write_all_bi", "write_all_bi");
        // DBI dbi = new DBI("jdbc:mysql://localhost:1167/EmployeeManagement",
        // "root", "iiit");
        h = dbi.open();
        h.begin();
        dao = h.attach(EmpPerDetaildao.class);
        // loginresponseobject tobereturned =
        //
        Details toBeReturned = new Details();

        // toBeReturned = dao.getTableValues(uuid);

        uuid = "1235";
        EmployeeDetails tempEmpDetail = new EmployeeDetails();
        // tempEmpDetail.getPersonalDetails() = dao.getPersonalData(uuid);

        // tempEmpDetail.setPersonalDetails(dao.getPersonalData(uuid));
        // tempEmpDetail.setAddressDetails(dao.getAddressData(uuid));
        tempEmpDetail.setFamilyDetails(dao.getFamilyData(uuid));
        // tempEmpDetail.setCommunicationDetails(dao.getCommunicationData(uuid));
        dao.getCommunicationData(uuid);

        System.out.println(tempEmpDetail.getFamilyDetails());
        System.out.println(tempEmpDetail.getCommunicationDetails());

        toBeReturned.setEmployeeDetails(tempEmpDetail);

        // PersonalDetails personalData = dao.getPersonalData(uuid);

        // toBeReturned = dao.getTableValues("11");
        // toBeReturned.employeeId = "sda";
        // toBeReturned.firstName = "rahul sharmaaa";

        h.commit();
        h.close();
        // System.out.println(toBeReturned.employeeId);
        return toBeReturned;
    }
}
