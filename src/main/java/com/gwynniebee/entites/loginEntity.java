package com.gwynniebee.entites;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.loginObject;
import com.gwynniebee.backoffice.objects.loginresponseobject;
import com.gwynniebee.dao.logindao;

public class loginEntity {

    public loginresponseobject authenticate(loginObject login_inst) throws ClassNotFoundException {
        Handle h = null;
        logindao dao = null;
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi = new DBI("jdbc:mysql://localhost:3306/EmployeeManagement", "root", "iiit");
        h = dbi.open();
        h.begin();
        dao = h.attach(logindao.class);
        loginresponseobject tobereturned = dao.authenticateuserandspecifyrole(login_inst.emailid, login_inst.password);
        h.commit();
        h.close();
        return tobereturned;
    }

}
