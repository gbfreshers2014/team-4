/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.backoffice.restlet;

import java.util.Properties;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.restlet.Application;
import org.restlet.data.MediaType;
import org.restlet.routing.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
import com.gwynniebee.iohelpers.IOGBUtils;
import com.gwynniebee.rest.service.restlet.GBRestletApplication;
import com.gwynniebee.backoffice.restlet.resources.leavesASR;
import com.gwynniebee.backoffice.restlet.resources.liquibaseASR;
import com.gwynniebee.backoffice.restlet.resources.loginASR;
import com.gwynniebee.backoffice.restlet.resources.searchASR;
=======
import com.gwynniebee.backoffice.restlet.resources.AddEmployee;
import com.gwynniebee.backoffice.restlet.resources.EmployeePersonalDetails;
import com.gwynniebee.backoffice.restlet.resources.liquibaseASR;
import com.gwynniebee.backoffice.restlet.resources.loginASR;
import com.gwynniebee.iohelpers.IOGBUtils;
import com.gwynniebee.rest.service.restlet.GBRestletApplication;
>>>>>>> initial push

/**
 * Barcode file upload service routing and controlling resources.
 * @author Sarath
 */

public class rest_api extends GBRestletApplication {

    private static final Logger LOG = LoggerFactory.getLogger(rest_api.class);
    private static Validator validator;
    private Properties serviceProperties;

    /**
     * (From Application.getCurrent)<br>
     * This variable is stored internally as a thread local variable and updated
     * each time a call enters an application.<br>
     * <br>
     * Warning: this method should only be used under duress. You should by
     * default prefer obtaining the current application using methods such as
     * {@link org.restlet.resource.Resource#getApplication()} <br>
     * @return The current application.
     */
    public static rest_api getCurrent() {
        return (rest_api) Application.getCurrent();
    }

    /*
     * @return
     * @see org.restlet.Application#createInboundRoot()
     */
    @Override
    public synchronized Router createInboundRoot() {

        Router router = super.createInboundRoot();
        String resourceUrl = null;
        this.getMetadataService().setDefaultMediaType(MediaType.APPLICATION_JSON);

        router.attach("/database_schema_sychronizer", liquibaseASR.class);
<<<<<<< HEAD
        router.attach("/employee-portal/authenticate.json", loginASR.class);
        router.attach("/employee-portal/employee_search.json",searchASR.class);
        router.attach("employee-portal/employees/${uuid}/leaves.json",leavesASR.class);
=======

        router.attach("/login/user_credentials.json", loginASR.class);

        router.attach("/employees/{UUID}.json", EmployeePersonalDetails.class);

        router.attach("/employees.json", AddEmployee.class);

>>>>>>> initial push
        LOG.debug("Attaching hello-world with " + resourceUrl);
        /*
         * { "emailid" : "ipsita@vishwakarma.com", "password" : "ipsipepsi" }
         */

        return router;
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#start()
     */
    @Override
    public synchronized void start() throws Exception {
        if (!this.isStarted()) {

            // Prepare Properties
            this.serviceProperties = IOGBUtils.getPropertiesFromResource("/emp_prop.properties");

            // Prepare validation factory
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            rest_api.validator = factory.getValidator();
        }
        // below will make this.isStarted() true
        super.start();
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#stop()
     */
    @Override
    public synchronized void stop() throws Exception {
        if (!this.isStopped()) {
            LOG.info("Application.stop()");
        }
        // below will make this.isStopped() true
        super.stop();
    }

    /**
     * @return the serviceProps
     */
    public Properties getServiceProperties() {
        return this.serviceProperties;
    }

    /**
     * @return validator
     */
    public static Validator getValidator() {
        return validator;
    }

}
