package com.gwynniebee.backoffice.restlet.resources;

//package com.gwynniebee.backoffice.objects;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
// import com.gwynniebee.rest.service.restlet.resources.*;
public class AddEmpObject {// extends AbstractResponse {
    // public String status = "";
    public String UUID = "";
    public String employeeId = "";
    public String firstName = "";
    public String lastName = "";
    public String DOB = "";
    public String DOJ = "";
    public String employmentStatus = "";// ENUM('employeed','retired'),
    public String created_on = "";// DATETIME not NULL,
    public String created_by = "";// char(36) not NULL,
    public String last_updated_on = "";// // DATETIME not NULL,
    public String last_updated_by = "";// _by char(36) not NULL,

    // public String PRIMARY KEY (UUID)

}
