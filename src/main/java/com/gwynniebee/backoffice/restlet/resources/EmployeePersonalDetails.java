package com.gwynniebee.backoffice.restlet.resources;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.slf4j.Logger;

import com.google.common.base.Strings;
import com.gwynniebee.backoffice.objects.StatusResponseObject;
import com.gwynniebee.backoffice.objects.UpdateInfoObject;
import com.gwynniebee.entites.PersonalDetailsEntity;
import com.gwynniebee.entites.UpdateDetailsEntity;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

//import com.gwynniebee.rest.service.restlet.resources.;
public class EmployeePersonalDetails extends AbstractServerResource {

    public String tempUuid = "";
    Logger log = org.slf4j.LoggerFactory.getLogger(EmployeePersonalDetails.class);

    @Override
    protected void doInit() {
        super.doInit();
        this.log.info("in_doInit");
        this.tempUuid = (String) this.getRequestAttributes().get("UUID");
        // this.tempUuid = "11";

        // System.out.println(this.tempUuid);
        if (Strings.isNullOrEmpty(this.tempUuid)) {
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        }
    }

    // public static void main(String[] a) throws JsonParseException,
    // ClassNotFoundException, IOException {
    // EmployeePersonalDetails aaa = new EmployeePersonalDetails();
    // aaa.getEmployee();
    //
    // }

    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginASR.class);

    // loginObject handles mail and password
    @Get
    public Details getEmployee() throws JsonParseException, IOException, ClassNotFoundException {

        Details response = new Details();

        // Get UUID here...............................
        // this.tempUuid = "12";
        // String uuid = this.tempUuid;

        // Process uuid from here.................
        // System.out.println(this.tempUuid + "dsad");
        // System.out.println(this.tempUuid);
        PersonalDetailsEntity temp = new PersonalDetailsEntity();
        response = temp.getPersonalInfo(this.tempUuid);
        //
        // System.out.println(response.firstName + response.UUID);
        //
        // // response.firstName = "rahul";
        // // response.UUID = this.tempUuid;
        return response;

    }

    @Put
    public StatusResponseObject updateEmployee(UpdateInfoObject obj) throws JsonParseException, IOException, ClassNotFoundException {
        StatusResponseObject response = new StatusResponseObject();

        UpdateDetailsEntity temp = new UpdateDetailsEntity();
        response = temp.updateDetails(obj);

        return response;
    }

    // @Post
    // public PersonalDetails a() {
    // }

}
