package com.gwynniebee.backoffice.restlet.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.FileSystemResourceAccessor;

import com.gwynniebee.rest.common.response.AbstractResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;

public class liquibasehandler extends AbstractResponse{
    void synchronizeDB() throws SQLException, LiquibaseException
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection con= DriverManager.getConnection("jdbc:mysql://192.168.1.100/Employee_Management","root","root");
        Liquibase liquibase = null;
        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(con));
        liquibase = new Liquibase("/log.xml", new FileSystemResourceAccessor(), database);
        liquibase.update(null);
    }
}
