package com.gwynniebee.backoffice.restlet.resources;


import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.restlet.data.Form;
import org.restlet.resource.Get;

import com.gwynniebee.entites.leavesEntityManager;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

public class leavesASR extends AbstractServerResource{
    @Get
    leavesresponse employeeSearch() throws ParseException, ClassNotFoundException
    {
        String uuid=(String)getRequest().getAttributes().get("word"); 
        Form fm=getRequest().getResourceRef().getQueryAsForm();
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date startDate=sdf1.parse(fm.getFirstValue("startDate"));
        java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime()); 
        java.util.Date endDate=sdf1.parse(fm.getFirstValue("endDate"));
        java.sql.Date sqlendDate = new java.sql.Date(endDate.getTime()); 
        
        leavesresponse leavesresp=leavesEntityManager.checkleaves(uuid,sqlStartDate,sqlendDate);
        leavesresp.setStatus(getRespStatus());
        return leavesresp;
    }   

}
