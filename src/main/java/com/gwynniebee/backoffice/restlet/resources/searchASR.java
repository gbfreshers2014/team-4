package com.gwynniebee.backoffice.restlet.resources;

import java.util.ArrayList;
import java.util.HashMap;

import org.restlet.data.Form;
import org.restlet.resource.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.searchResult;
import com.gwynniebee.backoffice.objects.searchResultArray;
import com.gwynniebee.entites.searchEntityManager;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

public class searchASR extends AbstractServerResource{
    private static final Logger LOG = LoggerFactory.getLogger(searchASR.class);
    @Get
    public searchHandler searchEmployee() throws ClassNotFoundException
    {
        
        ArrayList<String> parameters=new ArrayList<String>();
        parameters=getPatameters();
        HashMap<String,Integer> hm=new HashMap();
        searchHandler response=new searchHandler();
        int index=0;
        searchEntityManager sem=new searchEntityManager();
        for(String currentQueryParam :parameters)
        {
            LOG.debug("CURRENT:"+currentQueryParam);            
            searchResultArray sra=sem.searchQuery(currentQueryParam);
            for(searchResult sr : sra.list)
            {
                if(!hm.containsKey(sr.UUID))
                {
                    hm.put(sr.UUID,index++);
                    response.sra.list.add(sr);
                }
                else
                {
                    
                    response.sra.list.get(hm.get(sr.UUID)).value++;
                }                   
            }
        }
        response.list=response.sra.list;
        response.setStatus(getRespStatus());
        return response;
    }
    
    ArrayList<String> getPatameters()
    {
        ArrayList<String> queryParams=new ArrayList<String>();
        Form response=getRequest().getResourceRef().getQueryAsForm();
        String retrievedConcatenated=response.getValues("param");
        LOG.debug(retrievedConcatenated);
        for(String it:retrievedConcatenated.split(" "))
                {
                    queryParams.add(it);
                }
        return queryParams;
    }
}

