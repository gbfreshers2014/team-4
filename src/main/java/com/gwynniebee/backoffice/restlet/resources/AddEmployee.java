package com.gwynniebee.backoffice.restlet.resources;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.restlet.resource.Put;
import org.slf4j.Logger;

import com.gwynniebee.backoffice.objects.StatusResponseObject;
import com.gwynniebee.entites.AddEmpEntity;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

//import com.gwynniebee.rest.service.restlet.resources.;
public class AddEmployee extends AbstractServerResource {

    public String tempUuid = "";
    Logger log = org.slf4j.LoggerFactory.getLogger(AddEmployee.class);

    // @Override
    // protected void doInit() {
    // super.doInit();
    // this.log.info("in_doInit");
    // this.tempUuid = (String) this.getRequestAttributes().get("UUID");
    // // this.tempUuid = "11";
    //
    // // System.out.println(this.tempUuid);
    // if (Strings.isNullOrEmpty(this.tempUuid)) {
    // this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
    // }
    // }

    // public static void main(String[] a) throws JsonParseException,
    // ClassNotFoundException, IOException {
    // EmployeePersonalDetails aaa = new EmployeePersonalDetails();
    // aaa.getEmployee();
    //
    // }

    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginASR.class);

    // loginObject handles mail and password
    @Put
    public StatusResponseObject addEmp(AddEmpObject obj) throws JsonParseException, IOException, ClassNotFoundException {

        StatusResponseObject response = new StatusResponseObject();

        // Get UUID here...............................
        // this.tempUuid = "12";
        // String uuid = this.tempUuid;

        // Process uuid from here.................
        // System.out.println(this.tempUuid + "dsad");
        // System.out.println(this.tempUuid);
        AddEmpEntity temp = new AddEmpEntity();
        response = temp.addEmployee(obj);
        //
        // System.out.println(response.firstName + response.UUID);
        //
        // // response.firstName = "rahul";
        // // response.UUID = this.tempUuid;
        return response;

    }

    // @Post
    // public PersonalDetails a() {
    // }

}
