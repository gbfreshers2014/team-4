package com.gwynniebee.backoffice.restlet.resources;

//package com.gwynniebee.backoffice.objects;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.Status;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
// import com.gwynniebee.rest.service.restlet.resources.*;
public class Details {// extends AbstractResponse {

    Status status = new Status();
    EmployeeDetails employeeDetails = new EmployeeDetails();

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public EmployeeDetails getEmployeeDetails() {
        return this.employeeDetails;
    }

    public void setEmployeeDetails(EmployeeDetails employeeDetails) {
        this.employeeDetails = employeeDetails;
    }

    // public String PRIMARY KEY (UUID)

}
