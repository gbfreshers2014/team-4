package com.gwynniebee.backoffice.objects;

public class PersonalDetails {
    public String uuid = "";
    public String employeeId = "";
    public String email = "";
    public String firstName = "";
    public String lastName = "";
    // public String fatherName = "";
    // public String motherName = "";
    public String dob = "";
    public String doj = "";
    public String bloodGroup = "";
    public String employmentStatus = "";// ENUM('employeed','retired'),
    public String designation = "";
    // public String created_on = "";// DATETIME not NULL,
    // public String created_by = "";// char(36) not NULL,
    // public String last_updated_on = "";// // DATETIME not NULL,
    // public String last_updated_by = "";// _by char(36) not NULL

}
