package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.leavesdetails;
import com.gwynniebee.backoffice.restlet.resources.leavesresponse;

public class leavesMapper implements ResultSetMapper<leavesdetails> {

    @Override
    public leavesdetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        leavesresponse lr=new leavesresponse();
        lr.count=0;
            leavesdetails ld=new leavesdetails();
            ld.date=r.getDate("Date");
            ld.status=r.getString("status");
        return ld;
    }

}
