package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.StatusResponseObject;

//@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
//@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateDetailsMapper implements ResultSetMapper<StatusResponseObject> {
    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginMapper.class);
    @Override
    public StatusResponseObject map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        StatusResponseObject lro = new StatusResponseObject();
        // LOG.info("came here ");
        // LOG.info(r.getString("role") + r.getString("UUID"));
        if (r.first() == true) {
            lro.status = 0;
            lro.message = "updated_successfully";
        } else {
            lro = null;
        }

        // lro.firstName = r.getString("firstName");
        // lro.created_by = "raa";
        // lro.employeeId = r.getString("firstName");
        // System.out.println("sdadasdsadqs");
        return lro;
    }
}
