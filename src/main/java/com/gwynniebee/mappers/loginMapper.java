package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.loginresponseobject;

public class loginMapper implements ResultSetMapper<loginresponseobject> {
    private static final Logger LOG = LoggerFactory.getLogger(loginMapper.class);
    @Override
    public loginresponseobject map(int index, ResultSet r, StatementContext ctx) throws SQLException {
      loginresponseobject lro=new loginresponseobject();      
      LOG.info("came here ");
      LOG.info(r.getString("role")+r.getString("UUID"));
      if(r.first()==true)
      {
          lro.role=r.getString("role");
          lro.UUID=r.getString("UUID");
      }
      else
          lro=null;
      return lro;
    }

}
