package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.FamilyDetails;

//@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
//@JsonIgnoreProperties(ignoreUnknown = true)
public class EmpFamilyDetailsMapper implements ResultSetMapper<FamilyDetails> {
    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginMapper.class);
    @Override
    public FamilyDetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        FamilyDetails lro = new FamilyDetails();
        // LOG.info("came here ");
        // LOG.info(r.getString("role") + r.getString("UUID"));
        if (r.first() == true) {
            // lro.status = "000";
            // lro.created_on = r.getString("created_on");
            lro.fullName = r.getString("full_name");
            // lro.created_by = r.getString("created_by");
            lro.dependent = r.getBoolean("dependent");

            lro.dob = r.getString("dob");
            // lro.doj = r.getString("doj");
            // lro.employmentStatus = r.getString("employement_status");
            // lro.firstName = r.getString("first_name");
            // lro.lastName = r.getString("last_name");
            // lro.email = r.getString("email_id");
            lro.bloodGroup = r.getString("blood_group");
            lro.relation = r.getString("relation");

            // lro.last_updated_by = r.getString("last_updated_by");
            // lro.last_updated_on = r.getString("last_updated_on");

        } else {
            lro = null;
        }

        // lro.firstName = r.getString("firstName");
        // lro.created_by = "raa";
        // lro.employeeId = r.getString("firstName");
        // System.out.println("sdadasdsadqs");
        return lro;
    }
}
