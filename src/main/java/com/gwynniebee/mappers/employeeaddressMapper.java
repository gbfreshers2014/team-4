package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.address_details;

public class employeeaddressMapper implements ResultSetMapper<address_details>{

    @Override
    public address_details map(int index, ResultSet r, StatementContext ctx) throws SQLException {        
        address_details addr=new address_details();
        addr.street_addr=r.getString("street_address");
        addr.city=r.getString("city");
        addr.country_code=r.getString("country_code");
        addr.state=r.getString("state");
        addr.zip_code=r.getString("zip_code");
        return addr;
    }

}
