package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.communication_details;

public class employeecommunincationMapper implements ResultSetMapper<communication_details>{

    @Override
    public communication_details map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        communication_details cmd=new communication_details();
        cmd.detail=r.getString("details");
        cmd.type=r.getString("type");
        return cmd;
    }

}
