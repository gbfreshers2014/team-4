package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.family_details;

public class employeefamilyMapper implements ResultSetMapper<family_details>{

    @Override
    public family_details map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        family_details fmd=new family_details();
        fmd.bloodgroup=r.getString("blood_group");
        fmd.dependent=r.getBoolean("dependent");
        fmd.DOB=r.getDate("DOB");
        fmd.name=r.getString("full_name");
        return fmd;
    }

}
