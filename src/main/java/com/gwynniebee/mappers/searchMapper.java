package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.searchResult;
import com.gwynniebee.backoffice.objects.searchResultArray;

public class searchMapper implements ResultSetMapper<searchResult>{

    @Override
    public searchResult map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        searchResult sra=(new searchResult(r.getString("UUID"),r.getString("first_name"),r.getString("last_name"),r.getString("email_id")));
        sra.bloodgroup=r.getString("bloodgroup");
        sra.designation=r.getString("designation");
        sra.employment_status=r.getString("email_id");
        return sra;
    }
    
}
