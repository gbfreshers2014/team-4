package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.PersonalDetails;

//@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
//@JsonIgnoreProperties(ignoreUnknown = true)
public class EmpPersonalDetailsMapper implements ResultSetMapper<PersonalDetails> {
    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginMapper.class);
    @Override
    public PersonalDetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        PersonalDetails lro = new PersonalDetails();
        // LOG.info("came here ");
        // LOG.info(r.getString("role") + r.getString("UUID"));
        if (r.first() == true) {
            // lro.status = "000";
            lro.employeeId = r.getString("emp_id");
            // lro.created_by = r.getString("created_by");
            // lro.created_on = r.getString("created_on");
            lro.uuid = r.getString("uuid");
            lro.dob = r.getString("dob");
            lro.doj = r.getString("doj");
            lro.employmentStatus = r.getString("employement_status");
            lro.firstName = r.getString("first_name");
            lro.lastName = r.getString("last_name");
            lro.email = r.getString("email_id");
            lro.bloodGroup = r.getString("emp_blood_group");
            lro.designation = r.getString("designation");

            // lro.last_updated_by = r.getString("last_updated_by");
            // lro.last_updated_on = r.getString("last_updated_on");

        } else {
            lro = null;
        }

        // lro.firstName = r.getString("firstName");
        // lro.created_by = "raa";
        // lro.employeeId = r.getString("firstName");
        // System.out.println("sdadasdsadqs");
        return lro;
    }
}
