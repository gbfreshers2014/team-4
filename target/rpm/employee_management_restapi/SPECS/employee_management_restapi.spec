Name: employee_management_restapi
Version: 1.0.0
Release: SNAPSHOT20140723111926
Summary: employee_management_restapi
License: 2014, GwynnieBee
Distribution: REST template
Group: com.gwynniebee
autoprov: yes
autoreq: yes
BuildRoot: /home/rahul/Projects/login__application_flow/target/rpm/employee_management_restapi/buildroot

%description
Employee Management REST Services

%install
if [ -e $RPM_BUILD_ROOT ];
then
  mv /home/rahul/Projects/login__application_flow/target/rpm/employee_management_restapi/tmp-buildroot/* $RPM_BUILD_ROOT
else
  mv /home/rahul/Projects/login__application_flow/target/rpm/employee_management_restapi/tmp-buildroot $RPM_BUILD_ROOT
fi

ln -s employee_management_restapi-1.0.0-SNAPSHOT $RPM_BUILD_ROOT/home/gb/lib/employee_management_restapi
ln -s employee_management_restapi-1.0.0-SNAPSHOT-install.py $RPM_BUILD_ROOT/home/gb/bin/employee_management_restapi-install.py
ln -s employee_management_restapi-1.0.0-SNAPSHOT-uninstall.py $RPM_BUILD_ROOT/home/gb/bin/employee_management_restapi-uninstall.py

%files

%attr(755,root,root) /home/gb/bin/employee_management_restapi-1.0.0-SNAPSHOT-uninstall.py
%attr(755,root,root) /home/gb/bin/employee_management_restapi-1.0.0-SNAPSHOT-install.py
%attr(755,root,root) /home/gb/lib/employee_management_restapi-1.0.0-SNAPSHOT/employee_management_restapi-1.0.0-SNAPSHOT.war
%attr(755,root,root) /home/gb/lib/employee_management_restapi
%attr(755,root,root) /home/gb/bin/employee_management_restapi-install.py
%attr(755,root,root) /home/gb/bin/employee_management_restapi-uninstall.py
