#!/bin/sh
clear
new_project_name='employee_management_restapi'
new_properties_name='emp_prop'
local_tomcat_path='/var/lib/tomcat7'
package_name='backoffice'
resource_name='rest_api_resources'
application_name='rest_api'
project_description='Employee Management REST Services'

echo "running templates replace on REST-template Project"
rm -rf .git/
echo "replacing project name \"employee_management_restapi\" with \"$new_project_name\""
#grep -rl 'employee_management_restapi' .
grep -rl 'employee_management_restapi' . | xargs sed -i 's|employee_management_restapi|'"$new_project_name"'|g'

echo "replacing \"emp_prop.properties\" with \"$new_properties_name.properties\""
#grep -rl 'emp_prop.properties' .
grep -rl 'emp_prop.properties' . | xargs sed -i 's|emp_prop.properties|'"$new_properties_name.properties"'|g'
find ./configuration -name 'emp_prop.properties' -exec rename 'emp_prop.properties' $new_properties_name.properties '{}' \;

echo "replacing \"/var/lib/tomcat7\" with $local_tomcat_path"
#grep -rl '/var/lib/tomcat7'
grep -rl '/var/lib/tomcat7' . | xargs sed -i 's|/var/lib/tomcat7|'"$local_tomcat_path"'|g'

echo "replacing the description from \"{project-description}\" to \"$project_description\" in pom.xml"
sed -i 's|{project-description}|'"$project_description"'|g' pom.xml

echo "rearranging the files"
mv -v 'src/main/java/com/gwynniebee/{package-name}/restlet/resources/{resource-name}.java' 'src/main/java/com/gwynniebee/{package-name}/restlet/resources/'"$resource_name"'.java'
mv -v 'src/main/java/com/gwynniebee/{package-name}/restlet/{application-name}.java' 'src/main/java/com/gwynniebee/{package-name}/restlet/'"$application_name"'.java'
mv -v 'src/main/java/com/gwynniebee/{package-name}/' src/main/java/com/gwynniebee/$package_name/
mv -v 'src/test/java/com/gwynniebee/{package-name}/' src/test/java/com/gwynniebee/$package_name/

echo "changing the package name from \"{package-name}\" to \"$package_name\""
grep -rl '{package-name}' ./src
grep -rl '{package-name}' ./src | xargs sed -i 's|{package-name}|'"$package_name"'|g'

echo "replacing the resource name from \"RestTemplateResource\" to \"$resource_name\""
grep -rl '{resource-name}' ./src
grep -rl '{resource-name}' ./src | xargs sed -i 's|{resource-name}|'"$resource_name"'|g'

echo "replacing the Application name from \"{GBRestTemplateApplication}\" to \"$application_name\""
grep -rl '{application-name}' ./src
grep -rl '{application-name}' ./src | xargs sed -i 's|{application-name}|'"$application_name"'|g'

#rm template.sh~ template.sh
#end of shell script
#exit 0
